from cv2 import cv2
faceModel = cv2.CascadeClassifier('Opencv\haarcascade_frontalface_alt.xml')
capture = cv2.VideoCapture(0)

while True:
    ret ,frame = capture.read()
    #降维打击
    gray = cv2.cvtColor(frame,code=cv2.COLOR_RGB2GRAY)
    # 检测人脸，给出坐标
    faces = faceModel.detectMultiScale(gray,scaleFactor=1.2)
    # 标记人脸
    for (x,y,w,h) in faces:
        # 外边框
        cv2.rectangle(frame,pt1=(x,y),pt2=(x+w,y+h),color=(0,255,0),thickness=2)
        # 添加名字
        cv2.putText(frame,'Frank',org=(x,y-10),fontFace=cv2.FONT_HERSHEY_COMPLEX,fontScale=1,color=(0,255,255))
        # 显示图片
        cv2.imshow('LIVEface',frame)
    if cv2.waitKey(1) and 0xFF ==ord('q'):
        break
# 资源收回
capture.release()
# 关闭窗口
cv2.destroyAllWindows()