import requests
import sqlite3
import json
import random
import io  
import sys 
def find():
    sys.stdout = io.TextIOWrapper(sys.stdout.buffer,encoding='utf-8') 

    url = 'https://www.1905.com/api/content/index.php'
    header = {
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36'
    }
    # 请求参数
    parmas = {
        "callback": 'reloadList',
        "m": 'converged',
        "a": 'info',
        "type": 'jryp',
        "year": '2019',
        "month": '12',
    }
    # 请求地址
    response = requests.get(url = url,headers = header,params = parmas)
    result = response.text
    result = result.replace('reloadList(','').replace(')','')
    # 从json格式读取
    result = json.loads(result)
    # print(result)
    for i in result['info']:
        print(i['url'],i['title'],i['thumb'])
        # 读取照片
        img = requests.get(i['thumb']).content
        # 保存照片
        img_name = random.randint(10000,99999)
        with open('image/img%s.jpg'%img_name,'wb') as w:
            w.write(img)

        save_data(content = i['title'],link=i['url'],img=img_name)

def createDB():
    conn = sqlite3.connect('DB/film.db')
    c = conn.cursor()#执行命令
    c.execute('CREATE TABLE filmdata (id INTEGER PRIMARY KEY AUTOINCREMENT,content text ,link text,img text)')
    conn.commit()
    conn.close()

def save_data(content,link,img):
    conn = sqlite3.connect('DB/film.db')
    c = conn.cursor()#执行命令
    c.execute("INSERT into filmdata(content,link,img)VALUES('{0}','{1}','{2}')".format(content,link,img))
    conn.commit()
    conn.close()

def show_data():
    conn = sqlite3.connect('DB/film.db')
    c = conn.cursor()
    res = c.execute('SELECT * from filmdata')
    print(res)
    for i in res:
        print(i[2])
    conn.close()
if __name__ == "__main__":
    find()
    # createDB()
    show_data()